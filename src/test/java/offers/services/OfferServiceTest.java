package offers.services;

import offers.BaseTest;
import offers.exceptions.OfferValidationException;
import offers.model.Offer;
import offers.repositories.OfferRepository;
import offers.validator.OfferValidator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class OfferServiceTest extends BaseTest {

    @InjectMocks
    private OfferService OfferService = new OfferServiceImpl();

    @Mock
    private OfferValidator offerValidator;

    @Mock
    private OfferRepository repository;

    @Test
    public void shouldSaveOffer() throws Exception {

        // Given a new offer
        Offer offer = getValidOffer();
        doNothing().when(offerValidator).validate(offer);
        given(repository.save(offer)).willReturn(offer);

        // When an offer service saveOffer method is called
        Offer savedOffer = OfferService.saveOffer(offer);

        // Then a new offer should be saved
        Assert.assertThat(offer, is(savedOffer));
        verify(offerValidator).validate(offer);
        verify(repository).save(offer);

    }

    @Test(expected=OfferValidationException.class)
    public void shouldThrowValidationExceptionForOfferWithNoTitle() throws Exception {

        // Given a new offer
        Offer offer = getOfferWithNoTitle();
        doThrow(new OfferValidationException(new Exception(), "Offer title field is empty")).when(offerValidator).validate(offer);

        // When an offer service saveOffer method is called
        OfferService.saveOffer(offer);

        // Then an offer validation should throw an exception
        verify(offerValidator).validate(offer);
        verify(repository, times(0)).save(offer);

    }

    @Test(expected=OfferValidationException.class)
    public void shouldThrowValidationExceptionForOfferWithNoDescription() throws Exception {

        // Given a new offer
        Offer offer = getOfferWithNoDescription();
        doThrow(new OfferValidationException(new Exception(), "Offer description field is empty")).when(offerValidator).validate(offer);

        // When an offer service saveOffer method is called
        OfferService.saveOffer(offer);

        // Then an offer validation should throw an exception
        verify(offerValidator).validate(offer);
        verify(repository, times(0)).save(offer);

    }

    @Test
    public void shouldGetOffer() throws Exception {

        // Given a new offer
        Offer offer = getValidOffer();
        Optional<Offer> optionalOffer = Optional.of(offer);
        given(repository.findById(123L)).willReturn(optionalOffer);

        // When an offer service getOffer method is called
        Optional<Offer> actualOptionalOffer = OfferService.getOffer(123L);

        // Then a offer should be returned
        Assert.assertThat(optionalOffer, is(actualOptionalOffer));
        verify(repository).findById(123L);

    }

}
