package offers.acceptance;

import com.fasterxml.jackson.databind.ObjectMapper;
import offers.BaseTest;
import offers.controllers.OfferControllerImpl;
import offers.model.Offer;
import offers.model.OfferResponse;
import offers.services.OfferService;
import offers.utils.StatusUpdater;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

@RunWith(SpringRunner.class)
@WebMvcTest(OfferControllerImpl.class)
public class WithdrawOfferSpecificationTest extends BaseTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OfferService offerService;

    @MockBean
    private StatusUpdater statusUpdater;

    private String apiUrl = "/v1/offer/withdraw?id=1";

    @Value("classpath:input/offer.json")
    private Resource validOfferResource;

    @Test
    public void shouldWithdrawActiveOffer() throws Exception {

        // Given a valid offer
        saveOffer(validOfferResource);
        Offer offer = getValidOffer();
        Offer withdrawnOffer = getWithdrawnOffer();
        Optional<Offer> optionalOffer = Optional.of(offer);

        given(offerService.getOffer(123L)).willReturn(optionalOffer);
        given(statusUpdater.updateInactiveStatus(offer)).willReturn(withdrawnOffer);
        given(offerService.saveOffer(withdrawnOffer)).willReturn(withdrawnOffer);

        // When the api is called to withdraw an offer
        MvcResult result = mockMvc.perform(put(apiUrl)).andReturn();

        // Then an api response is successful
        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());

    }

    @Test
    public void shouldReturnOfferNotFound() throws Exception {

        // Given no offer exist

        // When the api is called to withdraw an offer
        MvcResult result = mockMvc.perform(put(apiUrl)).andReturn();

        // Then an api response is Not Found
        String contentAsString = result.getResponse().getContentAsString();
        OfferResponse offerResponse = new ObjectMapper().readValue(contentAsString, OfferResponse.class);
        assertEquals(HttpStatus.NOT_FOUND.value(), offerResponse.getError().getStatus());

    }

    private void saveOffer(Resource validOfferResource) throws Exception {
        mockMvc.perform(post(apiUrl).contentType(MediaType.APPLICATION_JSON).content(toString(validOfferResource))).andReturn();

    }

}
