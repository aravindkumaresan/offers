package offers.acceptance;

import com.fasterxml.jackson.databind.ObjectMapper;
import offers.BaseTest;
import offers.controllers.OfferControllerImpl;
import offers.exceptions.OfferValidationException;
import offers.model.Offer;
import offers.model.OfferResponse;
import offers.services.OfferService;
import offers.utils.StatusUpdater;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringRunner.class)
@WebMvcTest(OfferControllerImpl.class)
public class CreateOfferSpecificationTest extends BaseTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OfferService offerService;

    @MockBean
    private StatusUpdater statusUpdater;

    @Value("classpath:input/offer.json")
    private Resource validOfferResource;

    @Value("classpath:input/offer_no_title.json")
    private Resource OfferNoTitleResource;

    @Value("classpath:input/offer_no_description.json")
    private Resource OfferNoDescriptionResource;

    private String apiUrl = "/v1/offer";

    @Test
    public void shouldCreateOffer() throws Exception {

        // Given a valid offer
        Offer offer = getValidOffer();
        given(offerService.saveOffer(offer)).willReturn(offer);
        String validOfferResourceJson = toString(validOfferResource);

        // When the api is called to create an offer
        MvcResult result = mockMvc.perform(post(apiUrl).contentType(MediaType.APPLICATION_JSON).content(validOfferResourceJson)).andReturn();

        // Then an api response is successful
        Assert.assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
    }

    @Test
    public void shouldFailForUnsupportedMediaType() throws Exception {

        // Given an unsupported MediaType Application_XML
        String validOfferResourceJson = toString(validOfferResource);

        // When the api is called to create an offer
        MvcResult result = mockMvc.perform(post(apiUrl).contentType(MediaType.APPLICATION_XML).content(validOfferResourceJson)).andReturn();

        // Then an api response should return Unsupported MediaType code
        Assert.assertEquals(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value(), result.getResponse().getStatus());

    }


    @Test
    public void shouldReturnValidationErrorForOfferWithNoTitle() throws Exception {

        // Given an offer with no title
        Offer offer = getOfferWithNoTitle();
        when(offerService.saveOffer(offer)).thenThrow(new OfferValidationException(new Exception(), "Offer title field is empty"));
        String offerNoTitleResourceJson = toString(OfferNoTitleResource);

        // When the api is called to create an offer
        MvcResult result = mockMvc.perform(post(apiUrl).contentType(MediaType.APPLICATION_JSON).content(offerNoTitleResourceJson)).andReturn();
        String contentAsString = result.getResponse().getContentAsString();
        OfferResponse offerResponse = new ObjectMapper().readValue(contentAsString, OfferResponse.class);

        // Then an api response should return validation error
        Assert.assertEquals(HttpStatus.UNPROCESSABLE_ENTITY.value(), offerResponse.getError().getStatus());

    }

    @Test
    public void shouldReturnValidationErrorForOfferWithNoDescription() throws Exception {

        // Given an offer with no description
        Offer offer = getOfferWithNoDescription();
        when(offerService.saveOffer(offer)).thenThrow(new OfferValidationException(new Exception(), "Offer description field is empty"));
        String offerNoDescriptionResourceJson = toString(OfferNoDescriptionResource);

        // When the api is called to create an offer
        MvcResult result = mockMvc.perform(post(apiUrl).contentType(MediaType.APPLICATION_JSON).content(offerNoDescriptionResourceJson)).andReturn();
        String contentAsString = result.getResponse().getContentAsString();
        OfferResponse offerResponse = new ObjectMapper().readValue(contentAsString, OfferResponse.class);

        // Then an api response should return validation error
        Assert.assertEquals(HttpStatus.UNPROCESSABLE_ENTITY.value(), offerResponse.getError().getStatus());


    }
}
