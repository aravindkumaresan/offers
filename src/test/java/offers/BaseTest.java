package offers;

import offers.model.Offer;
import offers.model.OfferStatus;
import offers.model.OfferType;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.Resource;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Currency;

public class BaseTest {

    protected SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

    protected Offer getValidOffer() throws Exception {

        return new Offer("Mercedes Offer", "New Mercedes GLC 250 on Sale", OfferType.PRODUCT, new BigDecimal("100"), Currency.getInstance("GBP"), dateFormat.parse("27-03-2020 00:00:00"), OfferStatus.ACTIVE);
    }

    protected Offer getExpiredOffer() throws Exception {

        return new Offer("Mercedes Offer", "New Mercedes GLC 250 on Sale", OfferType.PRODUCT, new BigDecimal("100"), Currency.getInstance("GBP"), dateFormat.parse("20-03-2019 00:00:00"), OfferStatus.ACTIVE);
    }

    protected Offer getInActiveOffer() throws Exception {

        return new Offer("Mercedes Offer", "New Mercedes GLC 250 on Sale", OfferType.PRODUCT, new BigDecimal("100"), Currency.getInstance("GBP"), dateFormat.parse("20-03-2019 00:00:00"), OfferStatus.INACTIVE);
    }

    protected Offer getOfferWithNoTitle() throws Exception {

        return new Offer("", "New Mercedes GLC 250 on Sale", OfferType.PRODUCT, new BigDecimal("100"), Currency.getInstance("GBP"), dateFormat.parse("27-03-2020 00:00:00"), OfferStatus.ACTIVE);
    }

    protected Offer getOfferWithNoDescription() throws Exception {
        return new Offer("Mercedes Offer", "", OfferType.PRODUCT, new BigDecimal("100"), Currency.getInstance("GBP"), dateFormat.parse("27-03-2020 00:00:00"), OfferStatus.ACTIVE);
    }
    protected Offer getWithdrawnOffer() throws Exception {
        return new Offer("Mercedes Offer", "New Mercedes GLC 250 on Sale", OfferType.PRODUCT, new BigDecimal("100"), Currency.getInstance("GBP"), dateFormat.parse("27-03-2020 00:00:00"), OfferStatus.WITHDRAWN);
    }

    protected String toString(Resource resource) throws Exception {
        return IOUtils.toString(resource.getInputStream(), "UTF-8");
    }
}
