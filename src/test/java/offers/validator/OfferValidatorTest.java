package offers.validator;

import offers.BaseTest;
import offers.exceptions.OfferValidationException;
import offers.model.Offer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class OfferValidatorTest extends BaseTest {

    private OfferValidator validator = new OfferValidatorImpl();

    @Test
    public void shouldNotThrowExceptionForValidOffer() throws Exception {

        // Given a new offer
        Offer offer = getValidOffer();

        // When an offer validation is called
        validator.validate(offer);

        // Then an offer validation should not throw an exception

    }

    @Test(expected= OfferValidationException.class)
    public void shouldThrowValidationExceptionForOfferWithNoTitle() throws Exception {

        // Given a new offer
        Offer offer = getOfferWithNoTitle();

        // When an offer validation is called
        validator.validate(offer);

        // Then an offer validation should throw an exception

    }

    @Test(expected= OfferValidationException.class)
    public void shouldThrowValidationExceptionForOfferWithNoDescription() throws Exception {

        // Given a new offer
        Offer offer = getOfferWithNoDescription();

        // When an offer validation is called
        validator.validate(offer);

        // Then an offer validation should throw an exception

    }
}
