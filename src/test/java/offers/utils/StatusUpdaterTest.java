package offers.utils;

import offers.BaseTest;
import offers.model.Offer;
import offers.model.OfferStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class StatusUpdaterTest extends BaseTest {

    private StatusUpdater statusUpdater = new StatusUpdater();


    @Test
    public void shouldUpdateTheOfferStatusForExpiredOffer() throws Exception {

        // Given an expired offer
        Offer offer = getExpiredOffer();

        // When an offer status updater is called
        Offer updatedOffer = statusUpdater.updateInactiveStatus(offer);

        // Then an offer status should be updated to INACTIVE
        assertThat(OfferStatus.INACTIVE, is(updatedOffer.getStatus()));

    }

    @Test
    public void shouldNotUpdateTheOfferStatusForNonExpiredOffer() throws Exception {

        // Given an non expired offer
        Offer offer = getValidOffer();

        // When an offer status updater is called
        Offer updatedOffer = statusUpdater.updateInactiveStatus(offer);

        // Then an offer status should stay as ACTIVE
        assertThat(OfferStatus.ACTIVE, is(updatedOffer.getStatus()));

    }

    @Test
    public void shouldUpdateWithdrawStatusForActiveOffer() throws Exception {

        // Given an non expired offer
        Offer offer = getValidOffer();

        // When an offer status updater is called
        Offer updatedOffer = statusUpdater.updateWithdrawnStatus(offer);

        // Then an offer status should be updated to WITHDRAWN
        assertThat(OfferStatus.WITHDRAWN, is(updatedOffer.getStatus()));

    }

    @Test
    public void shouldNotUpdateWithdrawStatusForInActiveOffer() throws Exception {

        // Given an expired offer
        Offer offer = getExpiredOffer();

        // When an offer status updater is called
        Offer updatedOffer = statusUpdater.updateWithdrawnStatus(offer);

        // Then an offer status should be updated to INACTIVE
        assertThat(OfferStatus.INACTIVE, is(updatedOffer.getStatus()));

    }


}
