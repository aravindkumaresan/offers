package offers.controllers;

import offers.BaseTest;
import offers.exceptions.OfferValidationException;
import offers.model.Error;
import offers.model.Offer;
import offers.model.OfferResponse;
import offers.model.OfferStatus;
import offers.services.OfferService;
import offers.utils.StatusUpdater;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OfferControllerTest extends BaseTest {


    @InjectMocks
    private OfferController offerController = new OfferControllerImpl();

    @Mock
    private OfferService offerService;

    @Mock
    private StatusUpdater statusUpdater;

    @Test
    public void shouldCreateOffer() throws Exception {

        // Given a new offer
        Offer offer = getValidOffer();
        given(offerService.saveOffer(offer)).willReturn(offer);

        // When an offer controller's createOffer method is called
        OfferResponse response = offerController.createOffer(offer);

        // Then a new offer should be created
        List<Offer> offers = new ArrayList<>();
        offers.add(offer);
        Error error = new Error(0, "", "");
        OfferResponse expectedOfferResponse = new OfferResponse(offers, error);

        Assert.assertThat(response, is(expectedOfferResponse));
        verify(offerService).saveOffer(offer);

    }

    @Test
    public void shouldFailForOfferWithNoTitle() throws Exception {

        // Given a new offer
        Offer offer = getOfferWithNoTitle();
        when(offerService.saveOffer(offer)).thenThrow(new OfferValidationException(new Exception(), "Offer title field is empty"));

        // When an offer controller's createOffer method is called
        OfferResponse response = offerController.createOffer(offer);

        // Then a new offer should not be created
        Error error = new Error(422, "Validation error", "Offer title field is empty");
        OfferResponse expectedOfferResponse = new OfferResponse(null, error);

        Assert.assertThat(response, is(expectedOfferResponse));
        verify(offerService).saveOffer(offer);

    }

    @Test
    public void shouldFailForOfferWithNoDescription() throws Exception {

        // Given a new offer
        Offer offer = getOfferWithNoDescription();
        when(offerService.saveOffer(offer)).thenThrow(new OfferValidationException(new Exception(), "Offer description field is empty"));

        // When an offer controller's createOffer method is called
        OfferResponse response = offerController.createOffer(offer);

        // Then a new offer should not be created
        Error error = new Error(422, "Validation error", "Offer description field is empty");
        OfferResponse expectedOfferResponse = new OfferResponse(null, error);

        Assert.assertThat(response, is(expectedOfferResponse));
        verify(offerService).saveOffer(offer);

    }

    @Test
    public void shouldRetrieveAnActiveOfferById() throws Exception {

        // Given a new offer
        Offer offer = getValidOffer();
        Optional<Offer> optionalOffer = Optional.of(offer);

        given(offerService.getOffer(123L)).willReturn(optionalOffer);
        given(statusUpdater.updateInactiveStatus(offer)).willReturn(offer);

        // When an offer controller's getOffer method is called
        OfferResponse response = offerController.getOffer(123L);

        // Then a offer should be returned
        List<Offer> offers = new ArrayList<>();
        offers.add(offer);
        Error error = new Error(0, "", "");
        OfferResponse expectedOfferResponse = new OfferResponse(offers, error);

        Assert.assertThat(response, is(expectedOfferResponse));
        Assert.assertThat(response.getOffers().get(0).getStatus(), is(OfferStatus.ACTIVE));
        verify(offerService).getOffer(123L);
        verify(statusUpdater).updateInactiveStatus(offer);

    }

    @Test
    public void shouldRetrieveAnInActiveOfferById() throws Exception {

        // Given an expired offer
        Offer offer = getExpiredOffer();
        Offer inActiveOffer = getInActiveOffer();
        Optional<Offer> optionalOffer = Optional.of(offer);

        given(offerService.getOffer(123L)).willReturn(optionalOffer);
        given(statusUpdater.updateInactiveStatus(offer)).willReturn(inActiveOffer);

        // When an offer controller's getOffer method is called
        OfferResponse response = offerController.getOffer(123L);

        // Then an offer with InActive status should be returned
        List<Offer> offers = new ArrayList<>();
        offers.add(inActiveOffer);
        Error error = new Error(0, "", "");
        OfferResponse expectedOfferResponse = new OfferResponse(offers, error);

        Assert.assertThat(expectedOfferResponse, is(response));
        Assert.assertThat(response.getOffers().get(0).getStatus(), is(OfferStatus.INACTIVE));
        verify(offerService).getOffer(123L);
        verify(statusUpdater).updateInactiveStatus(offer);

    }

    @Test
    public void shouldReturnOfferNotFound() {

        // Given no offer exist
        Optional<Offer> optionalOffer = Optional.empty();

        given(offerService.getOffer(123L)).willReturn(optionalOffer);

        // When an offer controller's getOffer method is called
        OfferResponse response = offerController.getOffer(123L);

        // Then a offer should not be returned
        Error error = new Error(404, "Resource Not Found", "Resource Not Found");
        OfferResponse expectedOfferResponse = new OfferResponse(null, error);

        Assert.assertThat(response, is(expectedOfferResponse));
        verify(offerService).getOffer(123L);

    }

    @Test
    public void shouldAllowToWithdrawActiveOffer() throws Exception {

        // Given a new offer
        Offer offer = getValidOffer();
        Offer withdrawnOffer = getWithdrawnOffer();
        Optional<Offer> optionalOffer = Optional.of(offer);

        given(offerService.getOffer(123L)).willReturn(optionalOffer);
        given(statusUpdater.updateWithdrawnStatus(offer)).willReturn(withdrawnOffer);
        given(offerService.saveOffer(withdrawnOffer)).willReturn(withdrawnOffer);

        // When an offer controller's withdraw offer method is called
        OfferResponse response = offerController.withdrawOffer(123L);

        // Then a withdrawn offer should be returned
        List<Offer> offers = new ArrayList<>();
        offers.add(withdrawnOffer);
        Error error = new Error(0, "", "");
        OfferResponse expectedOfferResponse = new OfferResponse(offers, error);

        Assert.assertThat(expectedOfferResponse, is(response));
        Assert.assertThat(response.getOffers().get(0).getStatus(), is(OfferStatus.WITHDRAWN));
        verify(offerService).getOffer(123L);
        verify(statusUpdater).updateWithdrawnStatus(offer);
        verify(offerService).saveOffer(withdrawnOffer);

    }

    @Test
    public void shouldNotAllowToWithdrawInActiveOffer() throws Exception {

        // Given an InActive offer
        Offer inActiveOffer = getInActiveOffer();
        Optional<Offer> optionalOffer = Optional.of(inActiveOffer);

        given(offerService.getOffer(123L)).willReturn(optionalOffer);
        given(statusUpdater.updateWithdrawnStatus(inActiveOffer)).willReturn(inActiveOffer);
        given(offerService.saveOffer(inActiveOffer)).willReturn(inActiveOffer);

        // When an offer controller's withdraw offer method is called
        OfferResponse response = offerController.withdrawOffer(123L);

        // Then a withdrawn offer should be returned
        List<Offer> offers = new ArrayList<>();
        offers.add(inActiveOffer);
        Error error = new Error(0, "", "");
        OfferResponse expectedOfferResponse = new OfferResponse(offers, error);

        Assert.assertThat(expectedOfferResponse, is(response));
        Assert.assertThat(response.getOffers().get(0).getStatus(), is(OfferStatus.INACTIVE));
        verify(offerService).getOffer(123L);
        verify(statusUpdater).updateWithdrawnStatus(inActiveOffer);
        verify(offerService).saveOffer(inActiveOffer);

    }

}
