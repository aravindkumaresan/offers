package offers.validator;

import offers.exceptions.OfferValidationException;
import offers.model.Offer;

public interface OfferValidator {

    void validate(Offer offer) throws OfferValidationException;
}
