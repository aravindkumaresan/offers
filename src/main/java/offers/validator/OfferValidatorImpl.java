package offers.validator;

import offers.exceptions.OfferValidationException;
import offers.model.Offer;
import org.springframework.stereotype.Component;

@Component
public class OfferValidatorImpl implements OfferValidator {

    @Override
    public void validate(Offer offer) throws OfferValidationException {

       if(offer.getTitle().isEmpty()) {
           throw new OfferValidationException(new RuntimeException(), "Offer title field is empty");
       }

        if(offer.getDescription().isEmpty()) {
            throw new OfferValidationException(new RuntimeException(), "Offer description field is empty");
        }
    }
}
