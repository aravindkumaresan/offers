package offers.controllers;

import offers.exceptions.OfferValidationException;
import offers.model.Error;
import offers.model.Offer;
import offers.model.OfferResponse;
import offers.services.OfferService;
import offers.utils.StatusUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class OfferControllerImpl implements OfferController {

    @Autowired
    private OfferService offerService;

    @Autowired
    private StatusUpdater statusUpdater;

    @Override
    public OfferResponse createOffer(@RequestBody Offer offer) {

        try {

            Offer savedOffer = offerService.saveOffer(offer);
            List<Offer> offers = new ArrayList<>();
            offers.add(savedOffer);
            Error error = new Error(0, "", "");
            return new OfferResponse(offers, error);

        } catch (OfferValidationException ex) {

            Error error = new Error(422, "Validation error", ex.getMessage());
            return new OfferResponse(null, error);
        }
    }

    @Override
    public OfferResponse getOffer(long id) {

        Optional<Offer> optionalOffer = offerService.getOffer(id);
        if (optionalOffer.isPresent()) {

            List<Offer> offers = new ArrayList<>();

            Offer offer = optionalOffer.get();
            offers.add(statusUpdater.updateInactiveStatus(offer));
            Error error = new Error(0, "", "");
            return new OfferResponse(offers, error);
        } else {
            Error error = new Error(404, "Resource Not Found", "Resource Not Found");
            return new OfferResponse(null, error);
        }
    }

    @Override
    public OfferResponse withdrawOffer(long id) {

        try {

            Optional<Offer> optionalOffer = offerService.getOffer(id);

            if (optionalOffer.isPresent()) {
                Offer offer = optionalOffer.get();
                List<Offer> offers = new ArrayList<>();

                Offer statusUpdatedOffer = statusUpdater.updateWithdrawnStatus(offer);

                Offer savedOffer = offerService.saveOffer(statusUpdatedOffer);

                offers.add(savedOffer);
                Error error = new Error(0, "", "");
                return new OfferResponse(offers, error);
        } else {
            Error error = new Error(404, "Resource Not Found", "Resource Not Found");
            return new OfferResponse(null, error);
        }
        } catch (OfferValidationException ex) {

            Error error = new Error(422, "Validation error", ex.getMessage());
            return new OfferResponse(null, error);
        }
    }
}
