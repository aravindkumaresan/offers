package offers.controllers;

import offers.model.Offer;
import offers.model.OfferResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
interface OfferController {

    @RequestMapping(value = "/v1/offer", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    OfferResponse createOffer(Offer offer);

    @RequestMapping(value = "/v1/offer", method = RequestMethod.GET, produces = "application/json")
    OfferResponse getOffer(@RequestParam("id") long id);

    @RequestMapping(value = "/v1/offer/withdraw", method = RequestMethod.PUT, produces = "application/json")
    OfferResponse withdrawOffer(@RequestParam("id") long id);
}
