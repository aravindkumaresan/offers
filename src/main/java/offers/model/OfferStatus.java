package offers.model;

public enum OfferStatus {

    ACTIVE("Active"),
    INACTIVE("InActive"),
    WITHDRAWN("Withdrawn");

    private final String offerStatus;

    OfferStatus(String offerStatus)
    {
        this.offerStatus = offerStatus;
    }

    public String getOfferStatus() {
        return offerStatus;
    }
}

