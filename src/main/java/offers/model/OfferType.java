package offers.model;

public enum OfferType {

    PRODUCT("Product"),
    SERVICE("Service");

    private final String offerType;

    OfferType(String offerType)
    {
        this.offerType = offerType;
    }

    public String getOfferType() {
        return offerType;
    }
}

