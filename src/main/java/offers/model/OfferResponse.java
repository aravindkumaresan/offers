package offers.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown=true)
public class OfferResponse {

    private List<Offer> offers;
    private Error error;

    @JsonCreator
    public OfferResponse(@JsonProperty("offers") List<Offer> offers, @JsonProperty("error") Error error) {
        this.offers = offers;
        this.error = error;
    }

    public List<Offer> getOffers() {
        return offers;
    }

    public Error getError() {
        return error;
    }

    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OfferResponse that = (OfferResponse) o;
        return Objects.equals(offers, that.offers) &&
                Objects.equals(error, that.error);
    }

    @Override
    public int hashCode() {
        return Objects.hash(offers, error);
    }

}
