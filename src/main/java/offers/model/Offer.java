package offers.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.Date;
import java.util.Objects;

@Entity
public class Offer {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String title;
    private String description;
    private OfferType type;
    private BigDecimal price;
    private Currency currency;
    @JsonFormat(pattern = "dd-MM-yyyy hh:mm:ss")
    private Date validUntilDateTime;
    private OfferStatus status;

    public Offer() {

    }

    public Offer(String title, String description, OfferType type, BigDecimal price, Currency currency, Date validUntilDateTime, OfferStatus status) {
        this.title = title;
        this.description = description;
        this.type = type;
        this.price = price;
        this.currency = currency;
        this.validUntilDateTime = validUntilDateTime;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public OfferType getType() {
        return type;
    }

    public void setType(OfferType type) {
        this.type = type;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Date getValidUntilDateTime() {
        return validUntilDateTime;
    }

    public void setValidUntilDateTime(Date validUntilDateTime) {
        this.validUntilDateTime = validUntilDateTime;
    }

    public OfferStatus getStatus() {
        return status;
    }

    public void setStatus(OfferStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Offer offer = (Offer) o;
        return Objects.equals(id, offer.id) &&
                Objects.equals(title, offer.title) &&
                Objects.equals(description, offer.description) &&
                type == offer.type &&
                Objects.equals(price, offer.price) &&
                Objects.equals(currency, offer.currency) &&
                Objects.equals(validUntilDateTime, offer.validUntilDateTime) &&
                status == offer.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, description, type, price, currency, validUntilDateTime, status);
    }
}