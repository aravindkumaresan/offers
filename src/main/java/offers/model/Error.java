package offers.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Error {


    private int status;
    private String error;
    private String description;

    @JsonCreator
    public Error(@JsonProperty("status")int status,  @JsonProperty("error") String error, @JsonProperty("description") String description) {
        this.status = status;
        this.error = error;
        this.description = description;
    }

    public int getStatus() {
        return status;
    }

    public String getError() {
        return error;
    }

    public String getDescription() {
        return description;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setError(String error) {
        this.error = error;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Error error1 = (Error) o;
        return status == error1.status &&
                Objects.equals(error, error1.error) &&
                Objects.equals(description, error1.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status, error, description);
    }
}
