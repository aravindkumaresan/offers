package offers.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OfferValidationException extends Exception {

    private static final long serialVersionUID = 5774886428323848288L;
    private static final Logger logger = LoggerFactory.getLogger(OfferValidationException.class);

    public OfferValidationException (Throwable e, String message) {
        super (message, e);
        logger.error(getMessage(), e);
    }


}
