package offers.utils;

import offers.model.Offer;
import offers.model.OfferStatus;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class StatusUpdater {

    public Offer updateInactiveStatus(Offer offer) {

        if(offer.getValidUntilDateTime().before(new Date())) {
            offer.setStatus(OfferStatus.INACTIVE);
        }
        return offer;
    }

    public Offer updateWithdrawnStatus(Offer offer) {

        if(updateInactiveStatus(offer).getStatus().equals(OfferStatus.ACTIVE)) {
            offer.setStatus(OfferStatus.WITHDRAWN);
        }
        return offer;
    }
}
