package offers.repositories;


import offers.model.Offer;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface OfferRepository  extends CrudRepository<Offer, Long> {

    Optional<Offer> findById(Long id);
}
