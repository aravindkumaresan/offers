package offers.services;

import offers.exceptions.OfferValidationException;
import offers.model.Offer;
import offers.repositories.OfferRepository;
import offers.validator.OfferValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class OfferServiceImpl implements OfferService {

    @Autowired
    private OfferRepository repository;

    @Autowired
    private OfferValidator validator;

    public Offer saveOffer(Offer offer) throws OfferValidationException {

        validator.validate(offer);
        return repository.save(offer);

    }

    @Override
    public Optional<Offer> getOffer(long id) {

        return repository.findById(id);

    }
}