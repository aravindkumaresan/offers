package offers.services;

import offers.exceptions.OfferValidationException;
import offers.model.Offer;

import java.util.Optional;


public interface OfferService {

    Offer saveOffer(Offer offer) throws OfferValidationException;

    Optional<Offer> getOffer(long id);
}
