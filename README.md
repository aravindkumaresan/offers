Offers API
=========

Offers API is an RESTful service to be used by merchant's to offer their goods for sale.

Features
--------

- Merchant's can create an offer so that they can share it with their customers.
- Merchant's can retrieve the created offer along with status.
- Merchant's can withdraw an offer before it has expired.


API documentation
-----------------

Create Offer
------------

Verb - POST
URL - http://localhost:8080/v1/offer

Request:

Headers
    Content-Type:application/json
    
Body

{
  "title": "Mercedes Offer",
  "description": "New Mercedes GLC 250 on Sale",
  "type": "PRODUCT",
  "price": "100",
  "currency": "GBP",
  "validUntilDateTime": "27-03-2019 00:00:00",
  "status": "ACTIVE"
}

Response:

{
    "offers": [
        {
            "id": 1,
            "title": "Mercedes Offer",
            "description": "New Mercedes GLC 250 on Sale",
            "type": "PRODUCT",
            "price": 100,
            "currency": "GBP",
            "validUntilDateTime": "27-03-2019 12:00:00",
            "status": "ACTIVE"
        }
    ],
    "error": {
        "status": 0,
        "error": "",
        "description": ""
    }
}

Get Offer
------------

Verb - GET
URL - http://localhost:8080/v1/offer?id=1 

Response:

{
    "offers": [
        {
            "id": 1,
            "title": "Mercedes Offer",
            "description": "New Mercedes GLC 250 on Sale",
            "type": "PRODUCT",
            "price": 100,
            "currency": "GBP",
            "validUntilDateTime": "27-03-2019 12:00:00",
            "status": "ACTIVE"
        }
    ],
    "error": {
        "status": 0,
        "error": "",
        "description": ""
    }
}


Withdraw Offer
------------

Verb - PUT
URL - http://localhost:8080/v1/offer/withdraw?id=1 

Response:

{
    "offers": [
        {
            "id": 1,
            "title": "Mercedes Offer",
            "description": "New Mercedes GLC 250 on Sale",
            "type": "PRODUCT",
            "price": 100,
            "currency": "GBP",
            "validUntilDateTime": "27-03-2019 12:00:00",
            "status": "WITHDRAW"
        }
    ],
    "error": {
        "status": 0,
        "error": "",
        "description": ""
    }
}

Installation
------------
Requires the following software's to be setup to run this application.

- Java 1.8 or above
- Maven 3.6.0 (Maven bin directory to be set in path)

To run the application:  

mvn spring-boot:run


Known limitations
-----------------
- Experienced issues in mapping the PathVariable for getOffers so had to use RequestParam instead.
- Potential to refactor the controller further to make it clean. 
- No Authentication / Authorization support.
- No Payload Threat Protection / Security measures.
- Swagger API doc integration not done.
- h2 in-memory database is used for persistence.
- The project is not containerized using Docker.
